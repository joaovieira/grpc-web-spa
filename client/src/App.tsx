import * as React from 'react';
import { Router, Route, Redirect, IndexRoute, browserHistory } from 'react-router'
import ProjectList from './ProjectList';
import ProjectDetails from './ProjectDetails';
import NotFound from './NotFound';
import Layout from './Layout';

declare module "react-router/lib/Route" {
  interface RouteProps {
    name?: String;
    getDisplayName?: (name: String) => String,
  }
}

function App() {
  return (
    <Router history={browserHistory}>
      <Redirect from="/" to="/projects" />
      <Route path="/" component={Layout}>
        <Route name="All projects" path="projects">
          <IndexRoute component={ProjectList} />
          <Route name="Project details" path=":id" component={ProjectDetails} />
        </Route>
      </Route>
      <Route path="*" component={NotFound} />
    </Router>
  );
}

export default App;