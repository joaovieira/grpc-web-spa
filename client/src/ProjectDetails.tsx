import * as React from 'react';
import { grpc } from "grpc-web-client";
import { ProjectService } from "../_proto/project_service_pb_service";
import { Deployment, DeploymentsRequest } from "../_proto/project_service_pb";
import config from './config'

export interface Props {
  params: {
    id: string
  }
}

export interface State {
  loading: Boolean,
  invalidProject: Boolean,
  projectId: string,
  deployments: Array<Deployment>
}

class ProjectDetails extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      invalidProject: false,
      projectId: this.props.params.id,
      deployments: []
    }
  }

  componentDidMount() {
    this.getDeployments(this.state.projectId);
  }

  getDeployments(projectId: string) {
    const deployments: Array<Deployment> = [];
    const deploymentsRequest = new DeploymentsRequest();
    deploymentsRequest.setProjectName(projectId);

    this.setState({ loading: true });
    grpc.invoke(ProjectService.GetDeployments, {
      request: deploymentsRequest,
      host: config.serviceUrl,
      onMessage: (deployment: Deployment) => {
        deployments.push(deployment);
      },
      onEnd: (code: grpc.Code) => {
        this.setState({ loading: false });
        if (code == grpc.Code.OK) {
          this.setState({ deployments });
        } else {
          this.setState({ invalidProject: true });
        }
      }
    });
  }

  render() {
    const formatDeployment = (deployment: Deployment) => {
      const name = deployment.getDeploymentName();
      const isRunning = !!deployment.getStopTime() ? 'running' : 'stopped';
      return (
        <li key={name}>{name} ({isRunning})</li>
      );
    }

    if (this.state.loading) {
      return null;
    } else if (this.state.invalidProject) {
      return <div className="fullbleed">Project not found.</div>
    } else if (!this.state.deployments.length) {
      return <div className="fullbleed">There are no deployments for this project.</div>
    } else {
      return (
        <ul>
          {this.state.deployments.map(formatDeployment)}
        </ul>
      );
    }
  }
}

export default ProjectDetails;