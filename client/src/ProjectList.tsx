import * as React from 'react';
import { grpc } from "grpc-web-client";
import { Empty } from "google-protobuf/google/protobuf/empty_pb";
import { ProjectService } from "../_proto/project_service_pb_service";
import { Project, Deployment, DeploymentsRequest } from "../_proto/project_service_pb";
import { Link, RouteProps } from 'react-router';
import config from './config';
import ProjectDetails from './ProjectDetails';

export interface Props {
}

export interface State {
  loading: Boolean,
  projects: Array<Project>
}

class ProjectList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      loading: false,
      projects: []
    }
  }

  componentDidMount() {
    this.getProjects();
  }

  getProjects() {
    this.setState({ loading: true })
    const projects: Array<Project> = [];
    grpc.invoke(ProjectService.GetProjects, {
      request: new Empty(),
      host: config.serviceUrl,
      onMessage: (project: Project) => {
        projects.push(project);
      },
      onEnd: (code: grpc.Code) => {
        this.setState({ projects, loading: false });
      }
    });
  }

  render() {
    const generateLink = (project: Project) => {
      const name = project.getProjectName();
      return (
        <li key={name}>
          <Link to={`/projects/${encodeURIComponent(name)}`}>{name}</Link>
        </li>
      );
    }

    if (this.state.loading) {
      return null;
    } else if (!this.state.projects.length) {
      return <div className="fullbleed">There are no projects.</div>
    } else {
      return (
        <ul>
          {this.state.projects.map(generateLink)}
        </ul>
      );
    }
  }
}

export default ProjectList;