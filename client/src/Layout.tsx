import * as React from 'react';
import Breadcrumbs from 'react-breadcrumbs';
import { RouteComponentProps } from 'react-router';
import './Layout.css';

export interface Props extends RouteComponentProps<any, any> {
  children: React.ReactChildren
}

function Layout({ children, routes, params }: Props) {
  return (
    <div className="Layout">
      <Breadcrumbs displayMissing={false} routes={routes} params={params} />
      <div className="Main">
        {children}
      </div>
    </div>
  );
}

export default Layout;