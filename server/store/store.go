package store

import (
	"encoding/csv"
	"os"
	"path"

	pb "github.com/joaovieira/grpc-web-spa/server/_proto"
	"google.golang.org/grpc/grpclog"
)

const projectsFilename = "projects.csv"
const deploymentsRunningFilename = "deployments-running.csv"
const deploymentsStoppedFilename = "deployments-stopped.csv"

// Store is a raw data structure for all the data in CSV files.
type Store struct {
	Projects           []*pb.Project
	DeploymentsRunning []*pb.Deployment
	DeploymentsStopped []*pb.Deployment
}

// NewStore creates and returns a Store with all the data loaded
// from the given CSV files from dataDir.
func NewStore(dataDir string) *Store {
	projects, err := loadProjects(dataDir)
	if err != nil {
		grpclog.Fatalf("Failed to read projects: %v", err)
	}

	deploymentsRunning, err := loadDeploymentsRunning(dataDir)
	if err != nil {
		grpclog.Fatalf("Failed to read deployments running: %v", err)
	}

	deploymentsStopped, err := loadDeploymentsStopped(dataDir)
	if err != nil {
		grpclog.Fatalf("Failed to read deployments stopped: %v", err)
	}

	store := &Store{
		Projects:           projects,
		DeploymentsRunning: deploymentsRunning,
		DeploymentsStopped: deploymentsStopped,
	}
	return store
}

func loadProjects(dataDir string) ([]*pb.Project, error) {
	var projects []*pb.Project
	records, err := readCSV(path.Join(dataDir, projectsFilename))
	if err != nil {
		return nil, err
	}

	for _, record := range records[1:] {
		project := &pb.Project{
			ProjectName: record[0],
			Description: record[1],
		}
		projects = append(projects, project)
	}

	return projects, nil
}

func loadDeploymentsRunning(dataDir string) ([]*pb.Deployment, error) {
	var deployments []*pb.Deployment
	records, err := readCSV(path.Join(dataDir, deploymentsRunningFilename))
	if err != nil {
		return nil, err
	}

	for _, record := range records[1:] {
		deployment := &pb.Deployment{
			ProjectName:    record[0],
			DeploymentName: record[1],
			Stage:          record[2],
			StartTime:      record[3],
		}
		deployments = append(deployments, deployment)
	}

	return deployments, nil
}

func loadDeploymentsStopped(dataDir string) ([]*pb.Deployment, error) {
	var deployments []*pb.Deployment
	records, err := readCSV(path.Join(dataDir, deploymentsStoppedFilename))
	if err != nil {
		return nil, err
	}

	for _, record := range records[1:] {
		deployment := &pb.Deployment{
			ProjectName:    record[0],
			DeploymentName: record[1],
			Stage:          record[2],
			StartTime:      record[3],
			StopTime:       record[4],
		}
		deployments = append(deployments, deployment)
	}

	return deployments, nil
}

func readCSV(filePath string) ([][]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	defer file.Close()

	reader := csv.NewReader(file)

	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}
	return records, nil
}
