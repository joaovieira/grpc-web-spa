package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	pb "github.com/joaovieira/grpc-web-spa/server/_proto"
	"github.com/joaovieira/grpc-web-spa/server/projects"
	"github.com/joaovieira/grpc-web-spa/server/store"
)

var (
	port            = flag.Int("port", 9091, "The server port")
	dataDir         = flag.String("data_dir", "data", "Directory where CSV data files are stored")
	tlsCertFilePath = flag.String("tls_cert_file", "tools/cert.crt", "Path to the CRT/PEM file.")
	tlsKeyFilePath  = flag.String("tls_key_file", "tools/cert.key", "Path to the private key file.")
)

func main() {
	flag.Parse()

	store := store.NewStore(*dataDir)

	grpcServer := grpc.NewServer()
	pb.RegisterProjectServiceServer(grpcServer, &projects.ProjectService{store})
	grpclog.SetLogger(log.New(os.Stdout, "grpc-web-spa: ", log.LstdFlags))

	wrappedServer := grpcweb.WrapServer(grpcServer)
	handler := func(resp http.ResponseWriter, req *http.Request) {
		wrappedServer.ServeHTTP(resp, req)
	}

	httpServer := http.Server{
		Addr:    fmt.Sprintf(":%d", *port),
		Handler: http.HandlerFunc(handler),
	}

	grpclog.Printf("Starting server. https port: %d", *port)

	if err := httpServer.ListenAndServeTLS(*tlsCertFilePath, *tlsKeyFilePath); err != nil {
		grpclog.Fatalf("failed starting http2 server: %v", err)
	}
}
