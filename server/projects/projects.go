package projects

import (
	google_protobuf "github.com/golang/protobuf/ptypes/empty"
	pb "github.com/joaovieira/grpc-web-spa/server/_proto"
	"github.com/joaovieira/grpc-web-spa/server/store"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// ProjectService serves projects and nested deployment information.
type ProjectService struct {
	*store.Store // embedded pointer to db
}

// GetProjects streams all projects available in store.
func (s *ProjectService) GetProjects(_ *google_protobuf.Empty, stream pb.ProjectService_GetProjectsServer) error {
	for _, project := range s.Projects {
		if err := stream.Send(project); err != nil {
			return err
		}
	}
	return nil
}

// GetDeployments streams all deployments (running and stopped) for the given project.
func (s *ProjectService) GetDeployments(req *pb.DeploymentsRequest, stream pb.ProjectService_GetDeploymentsServer) error {
	validProject := false
	for _, project := range s.Projects {
		if req.ProjectName == project.ProjectName {
			validProject = true
			break
		}
	}

	if validProject == false {
		return grpc.Errorf(codes.NotFound, "Project does not exist")
	}

	deployments := append(s.DeploymentsRunning, s.DeploymentsStopped...)
	for _, deployment := range deployments {
		if req.ProjectName == deployment.ProjectName {
			if err := stream.Send(deployment); err != nil {
				return err
			}
		}
	}
	return nil
}
