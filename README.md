# grpc-web-spa

Typescript React SPA communicating via gRPC to Go backend.

**Backend**
- Go
- gRPC

**Frontend**
- React
- React Router
- TypeScript
- gRPC-web

(based on https://github.com/improbable-eng/grpc-web)

### Get started (HTTP/2 only)

* `yarn install`
* `yarn go-get-deps` to install Golang dependencies
* `yarn start` to start the Golang server and Webpack dev server
* Go to `https://localhost:8082`

### Next steps

* Fix typings for react-breadcrumbs (outdated)
* Add tests
* Build for production - client currently running with webpack dev server on separate port. Try serve built app from Go in same port using `soheilhy/cmux`(https://github.com/soheilhy/cmux) or `http.NewServeMux`.